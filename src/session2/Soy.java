package session2;

public class Soy extends CondimentDecorator {

    public Soy(Beverage beverage) {
        super(beverage);
        this.description = this.beverage.getDescription() + " with Soy";
    }

    public int baseCost(){
        return 300;
    }
}
