package session2_2;

import java.util.ArrayList;
import java.util.List;

public class OrderItem {
	private Beverage baseBeverage;
	private List<Condiment> addedConts;

	OrderItem(Beverage baseBeverage) {
		this.baseBeverage = baseBeverage;
		this.addedConts = new ArrayList<Condiment>();
	}

	public void addCondiment(Condiment condiment) {
		this.addedConts.add(condiment);
	}

	public int cost() {
		int res = baseBeverage.cost();
		for (Condiment condiment : addedConts) {
			res += condiment.cost();
		}
		return res;
	}

	public String getDescription() {
		StringBuilder res = new StringBuilder();
		res.append(this.baseBeverage.getDescription());
		for (int i = 0; i < this.addedConts.size(); i++) {
			if (i < (this.addedConts.size() - 1)) {
				res.append(" + " + this.addedConts.get(i).getDescription());
			} else {
				res.append(this.addedConts.get(i).getDescription());
			}
		}
		return res.toString();
	}
}
