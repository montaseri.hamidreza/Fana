package session2_2.condiments;

import session2_2.Condiment;

public class Soy extends Condiment {
	public Soy() {
		this.descrption = "Soy";
	}

	@Override
	public int cost() {
		return 15;
	}

}
