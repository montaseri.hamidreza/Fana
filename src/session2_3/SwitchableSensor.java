package session2_3;


public class SwitchableSensor extends SimpleSensor implements ISwitchableSensor {
    private boolean switchState;

    public SwitchableSensor (ISensorImplementation implementation) {
        super(implementation);
    }

    @Override
    public void switchOn() {
        this.switchState = true;
    }

    @Override
    public void switchOff() {
        this.switchState = false;
    }

    @Override
    public int getValue() {
        if(this.switchState) {
            return this.implementation.getValue();
        }
        else {
            return 0;
        }
    }
}
