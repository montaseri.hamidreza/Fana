package session1;

import java.security.InvalidParameterException;

public class Rectangle implements Shape {

	protected double height;
	protected double width;

	public Rectangle(double height, double width) {
		if (width < 0) {
			throw new InvalidParameterException("Invalid width in rectangle class");
		}
		if (height < 0) {
			throw new InvalidParameterException("Invalid height in rectangle class");
		}
		this.height = height;
		this.width = width;
	}

	@Override
	public double computeArea() {
		return this.getHeight() * this.getWidth();
	}

	public double getHeight() {
		return height;
	}

	public double getWidth() {
		return width;
	}

}
