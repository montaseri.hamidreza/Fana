package session2_3;

public class AveragingSensor extends SimpleSensor implements IAveragingSensor {
    private int currentValue = 0;
    private boolean averaging = false;


    public AveragingSensor(ISensorImplementation implementation) {
        super(implementation);
    }

    @Override
    public void beginAveraging() {
        this.averaging = true;
        this.currentValue = this.implementation.getValue();
    }

    @Override
    public int getValue() {
        this.currentValue += this.implementation.getValue();
        return currentValue;
    }
}
