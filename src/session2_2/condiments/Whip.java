package session2_2.condiments;

import session2_2.Condiment;

public class Whip extends Condiment {
	public Whip() {
		this.descrption = "Whip";
	}

	@Override
	public int cost() {
		return 10;
	}

}
