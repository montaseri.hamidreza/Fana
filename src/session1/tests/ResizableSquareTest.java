package session1.tests;

import java.security.InvalidParameterException;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import session1.ResizableSquare;


class ResizableSquareTest extends SquareTest {

	@BeforeAll
	static void build() {
		r = new ResizableSquare(4);
		expectedArea = 16;
	}

	@Test
	void testResize() {
		((ResizableSquare) r).resize(1);
		assertEquals(r.getHeight(), 1);
		assertEquals(r.getWidth(), 1);

		assertThrows(InvalidParameterException.class, () -> ((ResizableSquare) r).resize(-1));
	}

}