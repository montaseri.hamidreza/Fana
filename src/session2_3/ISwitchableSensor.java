package session2_3;

public interface ISwitchableSensor extends ISensor{
    void switchOn();
    void switchOff();
}
