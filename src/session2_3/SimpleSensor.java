package session2_3;

public class SimpleSensor implements ISensor{
    protected ISensorImplementation implementation;

    public SimpleSensor (ISensorImplementation implementation) {
        this.implementation = implementation;
    }

    @Override
    public int getValue() {
        return 11;
    }
}
