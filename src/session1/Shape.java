package session1;

public interface Shape {
	double computeArea();
}
