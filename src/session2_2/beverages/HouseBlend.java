package session2_2.beverages;

import session2_2.Beverage;

public class HouseBlend extends Beverage {
	public HouseBlend() {
		this.descrption = "HouseBlend";
	}

	@Override
	public int cost() {
		return 89;
	}

}
