package session2;

public class Whip extends CondimentDecorator {
    public Whip(Beverage beverage) {
        super(beverage);
        this.description = this.beverage.getDescription() + " with Whip";
    }

    public int baseCost(){
        return 200;
    }
}
