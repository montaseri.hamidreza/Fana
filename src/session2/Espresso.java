package session2;

public class Espresso extends ConcreteBeverage {

    public Espresso(){
        this.description = "Espresso";
    }

    public int cost(){
        return 1000;
    }
}
