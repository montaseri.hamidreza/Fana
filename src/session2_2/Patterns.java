package session2_2;

import session2_2.beverages.DarkRoast;
import session2_2.beverages.Espresso;
import session2_2.beverages.HouseBlend;
import session2_2.condiments.Mocha;
import session2_2.condiments.Soy;
import session2_2.condiments.Whip;

public class Patterns {

	private static void decoratorTestDrive() {
		OrderItem order = new OrderItem(new Espresso());
		System.out.println(order.getDescription() + " $" + order.cost());

		OrderItem order2 = new OrderItem(new DarkRoast());
		order2.addCondiment(new Mocha());
		order2.addCondiment(new Mocha());
		order2.addCondiment(new Whip());
		System.out.println(order2.getDescription() + " $" + order2.cost());

		OrderItem order3 = new OrderItem(new HouseBlend());
		order3.addCondiment(new Soy());
		order3.addCondiment(new Mocha());
		order3.addCondiment(new Whip());
		System.out.println(order3.getDescription() + " $" + order3.cost());
	}

	public static void main(String[] args) {
		// bridgeTestDrive();

		decoratorTestDrive();

	}
}
