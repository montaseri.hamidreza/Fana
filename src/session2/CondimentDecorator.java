package session2;

public abstract class CondimentDecorator extends  Beverage{
    Beverage beverage;

    public CondimentDecorator(Beverage beverage) {
        this.beverage = beverage;
    }

    public int cost() {
        return beverage.cost() + this.baseCost();
    }

    public abstract int baseCost();
}
