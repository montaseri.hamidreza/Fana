package session1;

import java.security.InvalidParameterException;

public class ResizableRectangle extends Rectangle {
	public ResizableRectangle(double height, double width) {
		super(height, width);
	}

	public void resize(double width, double height) {
		if (width < 0) {
			throw new InvalidParameterException("Invalid width in rectangle class");
		}
		if (height < 0) {
			throw new InvalidParameterException("Invalid height in rectangle class");
		}
		this.width = width;
		this.height = height;
	}
}
