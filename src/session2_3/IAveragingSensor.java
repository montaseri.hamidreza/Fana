package session2_3;

public interface IAveragingSensor  extends  ISensor {
    void beginAveraging();
}
