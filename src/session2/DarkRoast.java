package session2;

public class DarkRoast extends  ConcreteBeverage {

    public DarkRoast(){
        this.description = "Dark Roast";
    }

    public int cost(){
        return 10000;
    }
}
