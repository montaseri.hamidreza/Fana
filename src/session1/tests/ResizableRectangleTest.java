package session1.tests;

import java.security.InvalidParameterException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import session1.ResizableRectangle;


class ResizableRectangleTest extends RectangleTest {

	@BeforeAll
	static void build() {
		r = new ResizableRectangle(3, 4);
		expectedArea = 12;
	}

	@Test
	void testResize() {
		((ResizableRectangle) r).resize(8, 7);
		assertEquals(7, r.getHeight());
		assertEquals(8, r.getWidth());

		assertThrows(InvalidParameterException.class, () -> ((ResizableRectangle) r).resize(-1, 1));
		assertThrows(InvalidParameterException.class, () -> ((ResizableRectangle) r).resize(-1, -1));
		assertThrows(InvalidParameterException.class, () -> ((ResizableRectangle) r).resize(1, -1));
	}

}
