package session1;

import java.security.InvalidParameterException;

public class ResizableSquare extends Square {

	public ResizableSquare(double width) {
		super(width);

	}

	public void resize(double width) {
		if (width < 0) {
			throw new InvalidParameterException("Invalid width in square class");
		}
		this.width = width;
		this.height = width;
	}
}
