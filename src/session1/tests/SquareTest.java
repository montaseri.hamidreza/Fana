package session1.tests;

import org.junit.jupiter.api.BeforeAll;

import session1.Square;

class SquareTest extends RectangleTest {

	@BeforeAll
	static void build() {
		r = new Square(4);
		expectedArea = 16;
	}
}
