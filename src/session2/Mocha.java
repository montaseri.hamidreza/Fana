package session2;

public class Mocha extends CondimentDecorator {

    public Mocha(Beverage beverage) {
        super(beverage);

        this.description = this.beverage.getDescription() + " with Mocha";
    }

    public int baseCost(){
        return 500;
    }
}
