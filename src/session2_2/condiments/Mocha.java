package session2_2.condiments;

import session2_2.Condiment;

public class Mocha extends Condiment {

	public Mocha() {
		this.descrption = "Mocha";
	}

	@Override
	public int cost() {
		return 20;
	}

}
