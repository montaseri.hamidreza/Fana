package session1.tests;

import java.security.InvalidParameterException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import session1.Rectangle;

class RectangleTest {
	protected static Rectangle r;
	protected static double expectedArea, width, height;

	@BeforeAll
	static void build() {
		width = 3;
		height = 4;
		r = new Rectangle(width, height);
		expectedArea = 12;
	}

	@Test
	void testComputeArea() {
		assertEquals(expectedArea, r.computeArea());
	}

	@Test
	void testConstructor() {
		assertThrows(InvalidParameterException.class, () -> new Rectangle(-1, 5));
		assertThrows(InvalidParameterException.class, () -> new Rectangle(5, -2));
		assertThrows(InvalidParameterException.class, () -> new Rectangle(-1, -2));
	}
}
