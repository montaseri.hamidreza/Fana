package session2;

public class HouseBlend extends ConcreteBeverage {

    public HouseBlend(){
        this.description = "House Blend";
    }

    public int cost(){
        return 4000;
    }
}
